import xml.etree.ElementTree as ET
import argparse
import sys
import csv
import QualysAPI


def handleResponse(response: ET.Element):
    if response.find('responseCode') is None:
        print('FATAL: Response does not include a responseCode node')
        return False

    if response.find('responseCode').text == 'SUCCESS':
        return True

    print('ERROR: %s' % response.find('responseCode').text)
    print('Error message : %s' % response.find('responseErrorDetails/errorMessage').text)
    return False


def getAssets(api: QualysAPI.QualysAPI, filtercriteria: list = None):
    # Create our XML root which will be returned
    assets = ET.Element('data')

    # Set initial offset and record limit values for host asset requests and a flag to say when we've reached the
    #   end of the list
    offset = 1
    limit = 1000
    atend = False

    # Create the Service Request XML, used as the payload in the API call
    sr = ET.Element('ServiceRequest')
    filters = ET.SubElement(sr, 'filters')
    # Add any filters
    if filtercriteria is not None:
        criteria = ET.SubElement(filters, 'Criteria')
        criteria.set('field', filtercriteria[0])
        criteria.set('operator', filtercriteria[1])
        criteria.text = filtercriteria[2]

    prefs = ET.SubElement(sr, 'preferences')
    start = ET.SubElement(prefs, 'startFromOffset')
    lim = ET.SubElement(prefs, 'limitResults')
    lim.text = str(limit)

    fullurl = '%s/qps/rest/2.0/search/am/hostasset/?fields=id,name,dnsHostName' % api.server
    if api.debug:
        print('URL: %s' % fullurl)
    while not atend:
        # We set the starting location here because it will change with every iteration of this loop
        start.text = str(offset)
        # The payload is the contents of the ServiceRequest we send in the API call
        payload = ET.tostring(sr, method='html', encoding='utf-8').decode()
        if api.debug:
            print('PAYLOAD: %s' % payload)
        resp = api.makeCall(url=fullurl, payload=payload)

        # The response handler will tell us if there is a problem with the call
        # If there is, we delete the XML structures we created (good memory manangement) and return nothing
        if not handleResponse(resp):
            del sr, filters, prefs, start, lim, ha, resp, assets
            return None

        if resp.find('.//hasMoreRecords').text == 'true':
            # There are more records to process, so we increase the offset by the limit amount (the number of records
            #   we grab per call) so that the next call will start where the last call left off
            offset = offset + limit
            atend = False
        else:
            # hasMoreRecords is false, therefore this is the last call and we can stop after this iteration
            atend = True

        for ha in resp.findall('.//*HostAsset'):
            if api.debug:
                print('Processing %s' % ha.find('name').text)
            if ha.find('dnsHostName') is None:
                # This record has no dnsHostName attribute so we discard it and move on
                if api.debug:
                    print('Discarding, no dnsHostName attribute found')
                continue
            if ha.find('dnsHostName').text == ha.find('name').text:
                # This record has matching dnsHostName and name attributes, so we discard it and move on
                if api.debug:
                    print('Discarding, dnsHostName and name attributes match')
                continue
            # At this point we know that the asset has a dnsHostName attribute and that it does not match the name
            #   attribute.  We therefore add it to the assets list
            if api.debug:
                print('Candidate found')
            assets.append(ha)

    # A quick tidy up before we return the assets and leave this function
    del sr, filters, prefs, start, lim, ha, resp
    if api.debug:
        ET.dump(assets)
    return assets


def setAssetName(api: QualysAPI.QualysAPI, assetid: str, assetname: str):
    # Create our ServiceRequest which will be the payload of the API call to submit the name change
    sr = ET.Element('ServiceRequest')
    data = ET.SubElement(sr, 'data')
    ha = ET.SubElement(data, 'HostAsset')
    name = ET.SubElement(ha, 'name')
    name.text = assetname

    # Setup the API call, first with the URL to call and then with the payload
    fullurl = '%s/qps/rest/2.0/update/am/hostasset/%s' % (api.server, assetid)
    payload = ET.tostring(sr, method='html', encoding='utf-8').decode()

    # Now make the API call
    resp = api.makeCall(url=fullurl, payload=payload)
    if handleResponse(resp):
        del sr, data, ha, name, fullurl, payload, resp
        return True
    else:
        del sr, data, ha, name, fullurl, payload, resp
        return False


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('api_username', help='API Username in source subscription')
    parser.add_argument('api_password', help='API User Password in source subscription')
    parser.add_argument('qualyspod', help='Location of source Qualys Subscription [US01|US02|US03|EU01|EU02|IN01|PCP]')
    parser.add_argument('filename', help='Name of CSV file to create (with -C) or read (with -S)')
    parser.add_argument('-p', '--proxyenable', action='store_true', help='Use HTTPS Proxy (required -u or --proxyurl')
    parser.add_argument('-u', '--proxyurl', help='Proxy URL (requires -p or --proxyenable)')
    parser.add_argument('-f', '--field', help='Set the field on a search filter (also requires -o and -v)')
    parser.add_argument('-o', '--operator', help='Set the operator on a search filter (also requires -f and -v)')
    parser.add_argument('-v', '--value', help='Set the value on a search filter (also requires -f and -o)')
    parser.add_argument('-C', '--CreateList', action='store_true', help='Create CSV output file')
    parser.add_argument('-S', '--SetNames', action='store_true', help='Read CSV file and set asset names')
    parser.add_argument('-d', '--debug', action='store_true', help='Enable debug output')

    args = parser.parse_args()

    api_url = ''
    if args.qualyspod == 'PCP':
        if args.pcpurl is None:
            print('FATAL: qualyspod is PCP but apiurl is not specified')
            sys.exit(1)
        api_url = args.pcpurl
    else:
        api_url = QualysAPI.QualysAPI.podPicker(args.qualyspod)
        if api_url == 'invalid':
            print('FATAL: qualyspod \'%s\' is not recognised' % args.qualyspod)
            sys.exit(1)

    enableProxy = False
    proxyURL = ''
    if args.proxyenable:
        if args.proxyurl == '' or args.proxyurl is None:
            print('FATAL: -p or --proxyenable is specified without -u or --proxyurl')
            sys.exit(2)
        proxyURL = args.proxyurl
        enableProxy = True

    api = QualysAPI.QualysAPI(svr=api_url, usr=args.api_username, passwd=args.api_password, enableProxy=enableProxy,
                              proxy=proxyURL, debug=args.debug)

    filterspec = []
    if args.field is not None and args.operator is not None and args.value is not None:
        filterspec = [args.field, args.operator, args.value]

    if len(filterspec) > 0:
        assets = getAssets(api=api, filtercriteria=filterspec)
    else:
        assets = getAssets(api)

    if assets is None:
        # Something went wrong so we bail out here with a non-zero exit code
        exit(1)

    if args.CreateList:
        outfile = open(args.filename, 'w')
        data = assets.find('data')
        if api.debug:
            ET.dump(assets)
            print('%s records found in results' % len(assets.findall('HostAsset')))
        for asset in assets.findall('HostAsset'):
            outstr = '%s,%s,%s\n' % (asset.find('id').text, asset.find('name').text, asset.find('dnsHostName').text)
            outfile.write(outstr)
            print(outstr)
        outfile.close()
        print('Script complete, %s API calls made' % api.callCount)
        sys.exit(0)

    if args.SetNames:
        with open(args.filename, 'r') as csvfile:
            rowreader = csv.reader(csvfile, delimiter=',', quotechar='"')

            for row in rowreader:
                if not setAssetName(api, row[0], row[2]):
                    print('Could not set Asset Name for Asset ID %s' % row[0])
                    csvfile.close()
                    sys.exit(2)
        csvfile.close()
        print('Script complete, %s API calls made' % api.callCount)
        sys.exit(0)

